package com.coder.pojo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 员工档案表
 *
 * @author ZHT
 */
@Data
@TableName("history")
@Accessors(chain = true)
public class History {

    private static final long serialVersionUID = 1L;

    @TableId
    private Integer id;
    private Integer employeeNumber;
    private String name;
    private String gender;
    private Date birthday;
    private String telephone;
    private String email;
    private String address;
    private String photo;
    private String education;
    private Date inTime;
    private Date outTime;
    private Integer departmentNumber;
    private Integer positionNumber;
    private String status;
    private String home;
    private String notes;

    @TableField(exist = false)
    private Department department;
    @TableField(exist = false)
    private Position position;


}
