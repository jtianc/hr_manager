package com.coder.pojo;

import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
/**
 * (Wage)表实体类
 *
 * @author makejava
 * @since 2021-05-18 20:06:37
 */

public class Wage extends BasePojo {

    private Integer id;

    private Integer employeeNumber;

    private Object gangwei;

    private Object leave;

    private Object gongling;

    private Object xueli;

    private Object kaoqin;

    private Object shebao;

    private Object jixiao;

    private Object geren;

    private Object wage;


    /**
     * 获取主键值
     *
     * @return 主键值
     */

}
