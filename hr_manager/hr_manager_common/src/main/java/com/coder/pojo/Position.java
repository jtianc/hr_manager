package com.coder.pojo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 职称表
 *
 * @author ZHT
 */
@Data
@TableName("position")
public class Position {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer position_number;
    private String name;
    private String level;
    private String notes;


}
