package com.coder.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author WangYang
 * @date 2021/6/12 20:40
 */
@TableName("rights")
@Data
@Accessors(chain = true)
public class Rights extends BasePojo{
    private Integer id;
    private String name;
    private String path;
    private Integer parent_id;
    private Integer level;
    @TableField(exist = false)
    private List<Rights> children; //不是表格固有属性
}
