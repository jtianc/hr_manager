package com.coder.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
/**
 * (Employee)表实体类
 *
 * @author makejava
 * @since 2021-05-18 19:12:52
 */
@TableName("employee")
public class Employee extends BasePojo {

    private Integer id;

    private Integer employeeNumber;

    private String name;

    private Object gender;

    private Object birthday;

    private String telephone;

    private String email;

    private String address;

    private String photo;

    private String education;

    private Integer departmentNumber;

    private Integer positionNumber;

    private Object inTime;

    private String password;

    private String notes;


}
