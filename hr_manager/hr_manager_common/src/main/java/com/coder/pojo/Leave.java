package com.coder.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 请假表
 *
 * @author CJT
 */
@Data
@Accessors(chain = true)
@TableName("lea")
public class Leave{

    private static final long serialVersionUID = 1L;

    @TableId
    private Integer id;
    private Integer employeeNumber;
    private Integer departmentNumber;
    private Date startTime;
    private Date endTime;
    private String days;
    private String type;
    private String reason;
    private String manager;
    private Boolean status;
    private String notes;

    @TableField(exist = false)
    private Employee employee;
    @TableField(exist = false)
    private Department department;


}
