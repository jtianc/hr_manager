package com.coder.pojo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 员工调动记录表
 *
 * @author ZHT
 */
@Data
@TableName("move")
public class Move {

    private static final long serialVersionUID = 1L;

    @TableId
    private Integer id;
    private Integer employeeNumber;
    private Integer before;
    private Integer after;
    private Date time;
    private String manager;
    private String notes;

    @TableField(exist = false)
    private Employee employee;
    @TableField(exist = false)
    private Department department;
    @TableField(exist = false)
    private Department department2;


}
