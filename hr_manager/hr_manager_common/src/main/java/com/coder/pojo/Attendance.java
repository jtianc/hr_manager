package com.coder.pojo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 考勤表
 *
 * @author ZHT
 */
@Data
@Accessors(chain = true)
@TableName("attendance")
public class Attendance {


    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    private Integer day;
    private Integer mouth;
    private Integer year;
    private String created;
    private String startType;
    private String updated;
    private String endType;

    @TableField(exist = false)
    private Employee employee;


}
