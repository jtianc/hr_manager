package com.coder.pojo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 加班表
 *
 * @author ZHT
 */
@Data
@TableName("overtime")
public class Overtime {


    @TableId
    private Integer id;
    private Integer departmentNumber;
    private Integer employeeNumber;
    private Date day;
    private Date startTime;
    private Date endTime;
    private String notes;

    @TableField(exist = false)
    private Employee employee;
    @TableField(exist = false)
    private Department department;


}
