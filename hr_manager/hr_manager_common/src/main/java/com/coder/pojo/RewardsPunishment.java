package com.coder.pojo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
/**
 * (RewardsPunishment)表实体类
 *
 * @author makejava
 * @since 2021-05-18 19:23:28
 */
@SuppressWarnings("serial")
public class RewardsPunishment extends Model<RewardsPunishment> {

    private Integer id;

    private Integer employeeNumber;

    private String type;

    private String reason;

    private Object money;

    private Object time;

    private String manager;

    private String notes;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
