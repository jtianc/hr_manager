package com.coder.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 部门表
 *
 * @author ZHT
 */
@Data
@TableName("department")
public class Department {

    private static final long serialVersionUID = 1L;

    @TableId
    private Integer id;
    private Integer departmentNumber;
    private String name;
    private String manager;
    private String telephone;
    private String address;
    private String notes;


}
