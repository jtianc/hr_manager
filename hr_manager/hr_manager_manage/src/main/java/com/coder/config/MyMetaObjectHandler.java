package com.coder.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author WangYang
 * @date 2021/5/30 19:25
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    //入库操作时，需要更新 创建、更新时间
    @Override
    public void insertFill(MetaObject metaObject) {
        Date date = new Date();
        this.setFieldValByName("created", date,metaObject);
        this.setFieldValByName("updated", date,metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updated", new Date(),metaObject);

    }
}
