package com.coder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coder.pojo.Overtime;

public interface OvertimeMapper extends BaseMapper<Overtime> {

}
