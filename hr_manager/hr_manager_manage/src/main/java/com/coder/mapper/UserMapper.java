package com.coder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coder.pojo.User;

/**
 * @author WangYang
 * @date 2021/6/12 15:20
 */
public interface UserMapper extends BaseMapper<User> {
}
