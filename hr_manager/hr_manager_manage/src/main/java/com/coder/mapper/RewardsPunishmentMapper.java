package com.coder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coder.pojo.RewardsPunishment;

public interface RewardsPunishmentMapper extends BaseMapper<RewardsPunishment> {

}
