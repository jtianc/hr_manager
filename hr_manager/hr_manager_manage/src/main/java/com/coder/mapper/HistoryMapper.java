package com.coder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coder.pojo.History;

public interface HistoryMapper extends BaseMapper<History> {

}
