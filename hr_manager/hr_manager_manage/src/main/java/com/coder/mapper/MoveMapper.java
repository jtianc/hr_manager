package com.coder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coder.pojo.Move;

public interface MoveMapper extends BaseMapper<Move> {

}
