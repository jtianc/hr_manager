package com.coder.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coder.pojo.Employee;

public interface EmployeeMapper extends BaseMapper<Employee> {

	
}
