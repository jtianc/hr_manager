package com.coder.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coder.pojo.Leave;

public interface LeaveMapper extends BaseMapper<Leave> {

}
