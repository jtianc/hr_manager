package com.coder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coder.pojo.Rights;

/**
 * @author WangYang
 * @date 2021/6/12 20:44
 */
public interface RightMapper extends BaseMapper<Rights> {
}
