package com.coder.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coder.pojo.Department;

public interface DepartmentMapper extends BaseMapper<Department> {


}
