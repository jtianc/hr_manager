package com.coder.aop;

import com.coder.vo.SysResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author WangYang
 * @date 2021/5/30 19:24
 */
@RestControllerAdvice
public class AOPException {
    //实现的原理：异常通知
    //1.拦截什么类型的异常
    //2.拦截之后如何处理
    @ExceptionHandler({RuntimeException.class})
    public SysResult exception(Exception e){
        //将异常控制台输出
        e.printStackTrace();

        return SysResult.fail();
    }
}
