package com.coder.controller;


import com.coder.pojo.Department;
import com.coder.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/department")
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;
	
	@RequestMapping("/listPage.do")
	public String selectListByPgae(Model model, int pageNo){

		return "admin/department_list";
	}
	
	@RequestMapping("/toAdd.do")
	public String toAdd(Model model){

		return "admin/department_add";
	}
	
	@RequestMapping("/add.do")
	public String add(Department department){

		return "forward:/department/listPage.do?pageNo=1";
	}
	
	@RequestMapping("/{id}/toUpdate.do")
	public String toUpdate(@PathVariable Integer id, Model model){

		return "admin/department_update";
	}
	
	@RequestMapping("/{id}/update.do")
	public String updateById(@PathVariable Integer id, Department department){

		return "forward:/department/listPage.do?pageNo=1";
	}
	
	@RequestMapping("/{id}/delete.do")
	public String deleteById(@PathVariable Integer id){

		return "forward:/department/listPage.do?pageNo=1";
	}
	
}
