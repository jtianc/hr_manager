package com.coder.controller;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.coder.pojo.Department;
import com.coder.pojo.Overtime;
import com.coder.service.DepartmentService;
import com.coder.service.EmployeeService;
import com.coder.service.OvertimeService;
import com.coder.vo.PageResult;
import com.coder.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/overtime")
public class OvertimeController {

	@Autowired
	private OvertimeService overtimeService;


	@GetMapping("/findOverTimeByPage")
	public SysResult findAll(@RequestBody PageResult pageResult){
		pageResult = overtimeService.findByPage(pageResult);
		return SysResult.success(pageResult);
	}

	@PostMapping("/addOverTime")
	public SysResult addOverTime(@RequestBody Overtime overtime){
		overtimeService.insert(overtime);
		return SysResult.success();
	}

	@PutMapping("/updateOverTime")
	public SysResult updateOverTime(@RequestBody Overtime overtime){
		overtimeService.update(overtime);
		return SysResult.success();
	}

	@DeleteMapping("/{employeeNumber}")
	public SysResult deleteOverTime(@PathVariable Integer employeeNumber){
		overtimeService.delete(employeeNumber);
		return SysResult.success();
	}
	
}
