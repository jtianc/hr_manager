package com.coder.controller;



import javax.servlet.http.HttpSession;

import com.coder.pojo.Employee;
import com.coder.service.DepartmentService;
import com.coder.service.EmployeeService;
import com.coder.service.HistoryService;
import com.coder.service.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private PositionService positionService;
	@Autowired
	private HistoryService historyService;
	
	@RequestMapping("/login.do")
	public String toLogin(){
		return "login";
	}
	
	@RequestMapping("/checkLogin.do")
	public String checkLogin(HttpSession session, Employee employee){

			return "login";
	}
	
	@RequestMapping("/welcome.do")
	public String toWelcome(){
		return "welcome";
	}

	@RequestMapping("/listPage.do")
	public String selectList(Model model, int pageNo){

		return "admin/employee_list";
	}
	
	@RequestMapping("/{id}/detial.do")
	public String selectEmployee(@PathVariable Integer id, Model model){

		return "admin/employee_detail";
	}
	
	@RequestMapping("/toAdd.do")
	public String toAdd(Model model){

		return "admin/employee_add";
	}
	
	@RequestMapping("/add.do")
	public String add(Employee employee, String date) {

		return "forward:/employee/listPage.do?pageNo=1";
	}
	
	@RequestMapping("/{id}/toUpdate.do")
	public String toUpdate(Model model, @PathVariable Integer id){

		return "admin/employee_update";
	}
	
	@RequestMapping("/{id}/update.do")
	public String updateById(@PathVariable Integer id, Employee employee, String date, String status, 
			HttpSession session){

		return "forward:/employee/listPage.do?pageNo=1";
	}
	
	@RequestMapping("/{id}/delete.do")
	public String deleteById(@PathVariable Integer id){

		return "forward:/employee/listPage.do?pageNo=1";
	}
	
	@RequestMapping("/oneself/{id}/detial.do")
	public String selectEmployee2(@PathVariable Integer id, Model model){

		return "admin/oneself_detail";
	}
	
	@RequestMapping("/oneself/{id}/toUpdate.do")
	public String toUpdate2(Model model, @PathVariable Integer id){

		return "admin/oneself_update";
	}
	
	@RequestMapping("/search")
	public String search(Model model, String input, int pageNo){

		return "admin/search_result";
	}
	
	@RequestMapping("/logout.do")
	public String logout(HttpSession session){
		session.removeAttribute("loged");
		return "login";
	}
		
}
