package com.coder.controller;

import com.coder.pojo.User;
import com.coder.service.UserService;
import com.coder.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * @author WangYang
 * @date 2021/6/12 15:19
 */
@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @PostMapping("/login")
    public SysResult login(@RequestBody User user){

//        try {
//            String token = userService.findUserByUP(user);
//            if(StringUtils.hasLength(token)){
//                return SysResult.success(token);
//            }
//            else{
//                return SysResult.fail();
//            }
//        } catch (AOPException e) {
//            e.printStackTrace();
//            return SysResult.fail();
//
//        }
        String token = userService.findUserByUP(user);
        if(StringUtils.hasLength(token)){
            return SysResult.success(token);
        }
        else{
            return SysResult.fail();
        }
    }
}
