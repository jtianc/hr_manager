package com.coder.controller;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.coder.pojo.Employee;
import com.coder.pojo.History;
import com.coder.service.EmployeeService;
import com.coder.service.HistoryService;
import com.coder.util.MTimeUtil;
import com.coder.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
@RequestMapping("/history")
public class HistoryController {

	@Autowired
	private HistoryService historyService;
	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping("/findAll")
	public SysResult findAll(@RequestBody History history){
		historyService.find(history);
		return SysResult.success();
	}
	

}
