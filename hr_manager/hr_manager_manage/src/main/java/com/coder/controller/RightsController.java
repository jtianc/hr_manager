package com.coder.controller;

import com.coder.pojo.Rights;
import com.coder.service.RightService;
import com.coder.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author WangYang
 * @date 2021/6/12 20:46
 */
@RestController
@CrossOrigin
@RequestMapping("/right")
public class RightsController {
    @Autowired
    private RightService rightService;
    @GetMapping("/getRightsList")
    public SysResult getRightsList(){
        List<Rights> rightsList = rightService.findRightsList();
        return SysResult.success(rightsList);
    }
}
