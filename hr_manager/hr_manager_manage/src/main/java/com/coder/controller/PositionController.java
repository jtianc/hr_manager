package com.coder.controller;

import java.util.List;

import com.coder.pojo.Position;
import com.coder.service.PositionService;
import com.coder.vo.PageResult;
import com.coder.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/position")
public class PositionController {
	
	@Autowired
	private PositionService positionService;
	

	@GetMapping("/listPage.do")
	public SysResult selecListByPage(PageResult pageResult){
		pageResult=positionService.selectListByPage(pageResult);
		return SysResult.success(pageResult);
	}
	

	@PostMapping ("/toAdd.do")
	public SysResult toAdd(@RequestBody Position position){
		positionService.selectInster(position);
		return SysResult.success();
	}

	

}
