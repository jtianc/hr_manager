package com.coder.controller;


import com.coder.pojo.Attendance;
import com.coder.service.AttendanceService;
import com.coder.vo.PageResult;
import com.coder.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/attendance")
public class AttendanceController {

	@Autowired
	private AttendanceService attendanceService;

	@GetMapping("/hello")
	public List<Attendance> hello(){

		return attendanceService.findAll();
	}
	



	
	@GetMapping("/list")
	public SysResult selectList(PageResult pageResult){
		//查询所有考勤记录
		 pageResult = attendanceService.findByPage(pageResult);
		return SysResult.success(pageResult);
	}
	
	@GetMapping("/{employeeNumber}/oneself.do")
    public SysResult select( @PathVariable Integer employee_number){
		//查询一个员工的考勤记录
		attendanceService.findById(employee_number);
        return SysResult.success();
    }


}
