package com.coder.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.coder.pojo.Leave;
import com.coder.service.LeaveService;
import com.coder.util.MTimeUtil;
import com.coder.vo.PageResult;
import com.coder.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/leave")
public class LeaveController {

	@Autowired
	private LeaveService leaveService;

	@GetMapping("/list")
	public SysResult selectList(PageResult pageResult){
		pageResult = leaveService.findByPage(pageResult);
		return SysResult.success(pageResult);
	}

	@PutMapping("/status/{id}/{status}")
	public SysResult selectLeave(@RequestBody Leave leave){
		leaveService.updateLeaveStatus(leave);
		return SysResult.success();
	}

}


