package com.coder.service;

import com.coder.mapper.UserMapper;
import com.coder.pojo.User;
import org.springframework.stereotype.Service;

/**
 * @author WangYang
 * @date 2021/6/12 15:21
 */
public interface UserService {

    String findUserByUP(User user);
}
