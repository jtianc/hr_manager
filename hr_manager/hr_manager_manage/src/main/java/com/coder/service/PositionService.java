package com.coder.service;


import com.coder.pojo.Position;
import com.coder.vo.PageResult;

import java.util.List;

public interface PositionService {

    /**
     * 根据PositionNumber查询信息
     *
     * @param positionNumber
     * @return
     */
    Position selectByNumber(Integer positionNumber);

    /**
     * 分页查询所有职称（倒序）
     *
     * @param pageNo
     * @return
     */
    List<Position> selectListByPage(Integer pageNo);

    void selectInster(Position position);

    PageResult selectListByPage(PageResult pageResult);
}
