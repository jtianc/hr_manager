package com.coder.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.coder.pojo.Leave;
import com.coder.vo.PageResult;

import java.util.List;

public interface LeaveService {

    /**
     * 查询所有请假记录
     *
     * @param
     * @return
     */
    PageResult findByPage(PageResult pageResult);



    /**
     * 更改批准状态
     *
     * @param
     */
    void updateLeaveStatus(Leave leave);



}
