package com.coder.service.Impl;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.coder.pojo.Employee;
import com.coder.service.EmployeeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Override
    public Employee checkLogin(Integer employeeNumber, String password) {
        return null;
    }

    @Override
    public Page<Employee> selectListByPage(int pageNo) {
        return null;
    }

    @Override
    public void addEmployee(Employee employee) {

    }

    @Override
    public Employee selectEmployee(Integer id) {
        return null;
    }

    @Override
    public void updateEmployee(Employee employee, String status, String manager) {

    }

    @Override
    public void deleteEmployee(Integer id) {

    }

    @Override
    public Employee selectByNumber(Integer employeeNumber) {
        return null;
    }

    @Override
    public Page<Employee> search(String input, int pageNo) {
        return null;
    }

    @Override
    public List<Employee> select(Integer employeeNumber, String password) {
        return null;
    }
}
