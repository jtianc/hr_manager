package com.coder.service.Impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.coder.mapper.OvertimeMapper;
import com.coder.pojo.Overtime;
import com.coder.service.OvertimeService;
import com.coder.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.yaml.snakeyaml.events.Event;

import java.util.List;

@Service
public class OvertimeServiceImpl implements OvertimeService {

    @Autowired
    private OvertimeMapper overtimeMapper;



    @Override
    public PageResult findByPage(PageResult pageResult) {

        IPage<Overtime> page = new Page<>(pageResult.getPageNum(),pageResult.getPageSize());

        QueryWrapper<Overtime> queryWrapper = new QueryWrapper<>();
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"employeeNumber", pageResult.getQuery());
        page = overtimeMapper.selectPage(page, queryWrapper);
        long total = page.getTotal();
        List<Overtime> list = page.getRecords();
        return pageResult.setTotal(total).setRows(list);
    }

    @Override
    public void insert(Overtime overtime) {
        overtimeMapper.insert(overtime);
    }

    @Override
    public void update(Overtime overtime) {
        overtimeMapper.updateById(overtime);
    }

    @Override
    public void delete(Integer employeeNumber) {
        overtimeMapper.deleteById(employeeNumber);
    }
}



