package com.coder.service.Impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.coder.mapper.PositionMapper;
import com.coder.pojo.Position;
import com.coder.service.PositionService;
import com.coder.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class PositionServiceImpl implements PositionService {
    @Autowired
    private PositionMapper positionMapper;
    @Override
    public Position selectByNumber(Integer positionNumber) {
        return null;
    }

    @Override
    public List<Position> selectListByPage(Integer id) {
    return null;
    }

    @Override
    public void selectInster(Position position) {
            positionMapper.insert(position);

    }

    @Override
    public PageResult selectListByPage(PageResult pageResult) {
        IPage page = new Page(pageResult.getPageNum(), pageResult.getPageSize());
        QueryWrapper<Position> queryWrapper = new QueryWrapper<>();
        //用户是否传递参数
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"position_number", pageResult.getQuery());

        page = positionMapper.selectPage(page,queryWrapper);
        long total = page.getTotal();
        List<Position> itemList = page.getRecords();
        return pageResult.setTotal(total).setRows(itemList);
    }
}
