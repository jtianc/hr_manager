package com.coder.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.coder.pojo.Overtime;
import com.coder.vo.PageResult;

public interface OvertimeService {

    /**
     * 分页查询所有的加班记录
     *
     * @param
     * @return
     */
    PageResult findByPage(PageResult pageResult);


    /**
     * 查询一个员工的加班记录
     *
     * @return
     */

    //安排一个员工加班，即新增一个员工的加班记录

    void insert(Overtime overtime);




    //编辑一个员工的加班记录

    void update(Overtime overtime);

    //删除一个员工的加班记录

    void delete(Integer employeeNumber);

}
