package com.coder.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.coder.mapper.RightMapper;
import com.coder.pojo.Rights;
import com.coder.service.RightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author WangYang
 * @date 2021/6/12 20:45
 */
@Service
public class RightServiceImpl implements RightService {
    @Autowired
    private RightMapper rightMapper;


    @Override
    public List<Rights> findRightsList() {
        //查询一级菜单数据
        QueryWrapper<Rights> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id",0);
        List<Rights> oneList = rightMapper.selectList(queryWrapper);

        //查询二级菜单数据
        for (Rights oneRights: oneList) {
            QueryWrapper<Rights> queryWrapper2 = new QueryWrapper<>();
            queryWrapper2.eq("parent_id",oneRights.getId());
            List<Rights> twoList = rightMapper.selectList(queryWrapper2);
            oneRights.setChildren(twoList);
        }

        return oneList;

    }
}
