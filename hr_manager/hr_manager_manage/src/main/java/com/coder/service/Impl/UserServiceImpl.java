package com.coder.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.coder.mapper.UserMapper;
import com.coder.pojo.User;
import com.coder.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @author WangYang
 * @date 2021/6/12 15:22
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public String findUserByUP(User user) {
        //将密码加密,一般可能添加 盐值(一般由公司域名构成)
        // String password = DigestUtils.md5DigestAsHex(user.getPassword().getBytes());
        //2.根据用户名/密码查询数据库
        // user.setPassword(password);
        QueryWrapper<User> queryWrapper = new QueryWrapper<>(user);
        User selectOne = userMapper.selectOne(queryWrapper);

        String token = UUID.randomUUID().toString().replace("-", "");
//        if(selectOne != null){
//            return token;
//        }else{
//            return null;
//
//        }
        return selectOne!=null?token:null;

    }
}
