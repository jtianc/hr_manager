package com.coder.service;

import com.coder.pojo.Move;

import java.util.List;

public interface MoveService {

    /**
     * 查询所有的调动记录
     *
     * @return
     */
    List<Move> selectList();
}
