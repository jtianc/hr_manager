package com.coder.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.coder.mapper.LeaveMapper;
import com.coder.pojo.Attendance;
import com.coder.pojo.Leave;
import com.coder.service.LeaveService;
import com.coder.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class LeaveServiceImpl implements LeaveService {

    @Autowired
    private LeaveMapper leaveMapper;


    @Override
    public PageResult findByPage(PageResult pageResult) {
        IPage<Leave> page = new Page<>(pageResult.getPageNum(),pageResult.getPageSize());

        QueryWrapper<Leave> queryWrapper = new QueryWrapper<>();
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"employeeNumber", pageResult.getQuery());
        page = leaveMapper.selectPage(page, queryWrapper);
        long total = page.getTotal();
        List<Leave> leaveList = page.getRecords();
        return pageResult.setTotal(total).setRows(leaveList);
    }




    @Override
    public void updateLeaveStatus(Leave leave) {
        leaveMapper.updateById(leave);
    }




}
