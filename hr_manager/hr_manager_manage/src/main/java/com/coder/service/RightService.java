package com.coder.service;

import com.coder.pojo.Rights;

import java.util.List;

/**
 * @author WangYang
 * @date 2021/6/12 20:44
 */
public interface RightService {
    List<Rights> findRightsList();
}
