package com.coder.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.coder.mapper.AttendanceMapper;
import com.coder.pojo.Attendance;
import com.coder.service.AttendanceService;
import com.coder.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
@Service
public class AttendanceServiceImpl implements AttendanceService {

    @Autowired
    private AttendanceMapper attendanceMapper;

    @Override
    public PageResult findByPage(PageResult pageResult) {
        IPage<Attendance> page = new Page<>(pageResult.getPageNum(),pageResult.getPageSize());

        QueryWrapper<Attendance> queryWrapper = new QueryWrapper<>();
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"name", pageResult.getQuery());
        page = attendanceMapper.selectPage(page, queryWrapper);
        long total = page.getTotal();
        List<Attendance> attendanceList = page.getRecords();
        return pageResult.setTotal(total).setRows(attendanceList);

    }

    @Override
    public void findById(Integer employee_number) {
        QueryWrapper<Attendance> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("employee_number", employee_number);
        attendanceMapper.selectOne(queryWrapper);
    }

    @Override
    public List<Attendance> findAll() {
        return attendanceMapper.selectList(null);
    }


}
