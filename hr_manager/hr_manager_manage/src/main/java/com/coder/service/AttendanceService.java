package com.coder.service;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coder.pojo.Attendance;
import com.coder.vo.PageResult;
import org.springframework.stereotype.Service;

import java.util.List;
public interface AttendanceService{

    /**
     * 插入上班记录
     *
     *
     */

    /**
     * 更新下班记录
     *
     * @param
     */

    /**
     * 查询所有考勤记录
     *
     * @return
     */
    PageResult findByPage(PageResult pageResult);

    /**
     * 查询一个员工的考勤记录
     *
     * @return
     */
     void findById(Integer employeeNumber);


    List<Attendance> findAll();
}
