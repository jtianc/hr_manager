package com.coder.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.coder.pojo.Department;

public interface DepartmentService {

    /**
     * 根据DepartmentNumber查询信息
     *
     * @param departmentNumber
     * @return
     */
    Department selectByNumber(Integer departmentNumber);

    /**
     * 分页查询所有部门（倒序）
     *
     * @param pageNo
     * @return
     */
    Page<Department> selectListByPage(int pageNo);

}
