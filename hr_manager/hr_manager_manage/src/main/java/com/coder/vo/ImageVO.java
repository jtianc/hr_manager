package com.coder.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ImageVO implements Serializable {
    private String virtualPath; //图片存储的路径 不包含磁盘信息
    private String urlPath;//图片访问的网络地址
    private String fileName;//图片名称

}
