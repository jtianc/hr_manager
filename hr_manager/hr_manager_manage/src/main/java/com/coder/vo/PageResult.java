package com.coder.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author WangYang
 * @date 2021/5/26 8:01
 */
@Data
@Accessors(chain = true)
public class PageResult {
    private String query;
    private Integer pageNum;
    private Integer pageSize;
    private Long total;
    private Object rows;
}